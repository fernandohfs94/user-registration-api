
# User Registration API

API é responsável por gerenciar usuários através do banco de dados

## Sumário

- [Pré-requisitos](#pré-requisitos)
- [Executar localmente](#executar-localmente)
    - [Informações importantes](#informações-importantes)
- [Variáveis de ambiente](#variáveis-de-ambiente)
- [Documentação](#documentação)
- [Testes](#rodando-os-testes)

## Pré-requisitos

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)
- [Docker](https://www.docker.com/get-started)
- [Docker Compose](https://docs.docker.com/compose/cli-command/)

## Executar localmente

Faça um clone do projeto

#### https
```bash
  git clone https://gitlab.com/fernandohfs94/user-registration-api
```

#### ssh
```bash
  git clone git@gitlab.com:fernandohfs94/user-registration-api.git
```

Vá para o diretório do projeto

```bash
  cd user-registration-api
```

Instale as dependências

```bash
  yarn
```

Crie um arquivo `.env` com as variáveis de ambiente preenchidas (de acordo com o arquivo `.env.example`)

Suba o container com as configurações do banco

```bash
  docker-compose up -d
```

Rode as migrations

```bash
  yarn migrate
```

Inicie o servidor

```bash
  yarn dev
```

### Informações importantes

Após rodar o projeto localmente, para conseguir consumir os endpoints relativos ao `/user`, será necessário informar um `token JWT` no `header` da requisição como no exemplo abaixo

```js
  headers: {
    "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ"
  }
```

Para obter o token basta preencher as variáveis de ambiente `JWT_TOKEN` e `JWT_SECRET_KEY` no arquivo `.env`, acessar o site [jwt.io](https://jwt.io/) e no campo `PAYLOAD: DATA` adicionar o mesmo valor da variável `JWT_TOKEN` como no exemplo abaixo

<img src="./docs/payload-jwt.jpeg" />

## Variáveis de ambiente

Para executar este projeto, você precisará adicionar as seguintes variáveis de ambiente ao seu arquivo `.env`

| Nome | Descrição | Valor Padrão | Exemplo | Obrigatório |
| -- | -- | -- | -- | -- |
| NODE_ENV | Ambiente em que o app está rodando (development, staging ou production) | | development | :white_check_mark: |
| HOST | Host em que a aplicação rodará | | localhost | :white_check_mark: |
| PORT | Porta do host em que a aplicação rodará | | 3333 | :white_check_mark: |
| JWT_TOKEN | Token de autenticação na aplicação | | jwtDevelopment | :white_check_mark: |
| JWT_SECRET_KEY | Chave secreta do token JWT | | 15cc41c9fa82806a0b7ef2ada7c86574 | :white_check_mark: |
| DB_NAME | Nome do banco de dados | | user-registration-api | :white_check_mark: |
| DB_HOST | Host em que o banco de dados está rodando | | localhost | :white_check_mark: |
| DB_PORT | Porta do host em que o banco de dados está rodando | | 3306 | :white_check_mark: |
| DB_USERNAME | Nome do usuário do banco de dados | | user-registration-api | :white_check_mark: |
| DB_PASSWORD | Senha do usuário do banco de dados | | user-registration-api | :white_check_mark: |

## Documentação

Para visualizar a documentação da API basta acessar [este link](https://user-registration-api-fernando.herokuapp.com/documentation).

Para consumir o recurso `/users`, utilize o seguinte token

```bash
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dFByb2R1Y3Rpb24iLCJpYXQiOjE1MTYyMzkwMjJ9.mT1yJ56tHhC5Q3Y-hB_7faztO2tU3NQHHd1Z1z6YNGU
```

## Rodando os testes

Para rodar os testes, execute o seguinte comando na pasta raiz do projeto

```bash
  yarn test
```
