import { Request, ResponseToolkit } from '@hapi/hapi';
import HTTPStatus from 'http-status';

import { InternalServerErrorException } from '../../config/exception/http-exception';

class HealthController {
  async getBalance(request: Request, h: ResponseToolkit) {
    try {
      return h.response({
        status: 'ok',
      });
    } catch (e) {
      return h
        .response(new InternalServerErrorException().getResponse())
        .code(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

export default new HealthController();
