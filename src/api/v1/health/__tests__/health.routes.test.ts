import Server from '../../../../server';

describe('Route /health/check', () => {
  const server = Server.getInstance().getServer();

  beforeAll(done => {
    server.events.on('start', () => {
      done();
    });
  });

  afterAll(done => {
    server.events.on('stop', () => {
      done();
    });

    server.stop();
  });

  test('GET / - verify application health', async () => {
    const options = {
      method: 'GET',
      url: '/v1/health/check',
    };

    const response = await server.inject(options);

    expect(response.payload).toMatchSnapshot();
    expect(response.statusCode).toBe(200);
  });
});
