import { ServerRoute } from '@hapi/hapi';

import HealthController from './health.controller';
import { responses } from './health.schema';

const healthRoutes: ServerRoute[] = [
  {
    method: 'GET',
    path: '/v1/health/check',
    options: {
      auth: false,
      handler: HealthController.getBalance,
      description: 'Check if the application is running successfully',
      tags: ['api', 'health'],
      plugins: {
        'hapi-swagger': {
          responses,
        },
      },
    },
  },
];

export default healthRoutes;
