import Joi from 'joi';

import { InternalServerErrorSchema } from '../../config/exception/schema/http-exeption.schema';

const schema = Joi.object({
  status: Joi.string().default('ok'),
}).label('Health Check Response');

export const responses = {
  200: {
    description: 'The application is running successfully',
    schema,
  },
  500: {
    schema: InternalServerErrorSchema,
  },
};
