class UserNotUpdatedException extends Error {
  constructor(message: string) {
    super(message);
  }
}

export default UserNotUpdatedException;
