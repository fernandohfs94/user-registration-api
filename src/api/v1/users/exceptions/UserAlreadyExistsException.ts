class UserAlreadyExistsException extends Error {
  constructor(message: string) {
    super(message);
  }
}

export default UserAlreadyExistsException;
