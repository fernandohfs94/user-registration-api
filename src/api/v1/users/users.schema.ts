import Joi from 'joi';

import {
  ConflictErrorExceptionSchema,
  InternalServerErrorSchema,
  NotFoundErrorExceptionSchema,
  ServiceUnavailableErrorExceptionSchema,
} from '../../config/exception/schema/http-exeption.schema';

const schemas = {
  request: Joi.object({
    login: Joi.string().example('ex_ample').required(),
    password: Joi.string().example('123456').required(),
    fullName: Joi.string().example('Example of Full Name').required(),
    document: Joi.string().example('12345678901').required(),
    address: Joi.object({
      zipCode: Joi.string().example('14412206').required(),
      street: Joi.string().example('Rua Jovita').required(),
      number: Joi.number().example(462).required(),
      district: Joi.string().example('Jardim das Palmeiras').required(),
      city: Joi.string().example('Franca').required(),
      state: Joi.string().example('SP').required(),
    })
      .required()
      .label('User Address Request Payload'),
  })
    .required()
    .label('User Request Complete Payload'),
  requestUpdate: Joi.object({
    login: Joi.string(),
    password: Joi.string(),
    fullName: Joi.string(),
    document: Joi.string(),
    address: Joi.object({
      zipCode: Joi.string(),
      street: Joi.string(),
      number: Joi.number(),
      district: Joi.string(),
      city: Joi.string(),
      state: Joi.string(),
    }).label('User Address Request Update Payload'),
  })
    .required()
    .label('User Request Update Complete Payload'),
  response: Joi.object({
    id: Joi.number().example(1),
    uuid: Joi.string().uuid().example('d08f711e-c49a-4f2c-a734-49e8f305c21c'),
    login: Joi.string().example('fq_soares'),
    fullName: Joi.string().example('Fernando Henrique Ferreira Soares'),
    document: Joi.string().example('12345678901'),
    address: Joi.object({
      zipCode: Joi.string().example('14412206'),
      street: Joi.string().example('Rua Jovita'),
      number: Joi.number().example(462),
      district: Joi.string().example('Jardim das Palmeiras'),
      city: Joi.string().example('Franca'),
      state: Joi.string().example('SP'),
    }).label('User Address Response Payload'),
    createdAt: Joi.date().example('2021-19-06T00:00:00:000'),
    updatedAt: Joi.date().example('2021-19-06T00:00:00:000'),
    active: Joi.boolean().example(true),
  }).label('User Response Payload'),
  responseList: Joi.array()
    .items(
      Joi.object({
        id: Joi.number().example(1),
        uuid: Joi.string()
          .uuid()
          .default('d08f711e-c49a-4f2c-a734-49e8f305c21c'),
        login: Joi.string().example('fq_soares'),
        fullName: Joi.string().example('Fernando Henrique Ferreira Soares'),
        document: Joi.string().example('12345678901'),
        createdAt: Joi.date().example('2021-19-06T00:00:00:000'),
        updatedAt: Joi.date().example('2021-19-06T00:00:00:000'),
        active: Joi.boolean().example(true),
      }).label('User Object from List Payload'),
    )
    .label('User Response List Payload'),
  query: Joi.object({
    fullName: Joi.string(),
    login: Joi.string(),
    document: Joi.string(),
  }).label('Query'),
};

const responses = {
  create: {
    201: {
      description: 'The user was create successfully',
      schema: schemas.response,
    },
    409: {
      schema: ConflictErrorExceptionSchema,
    },
    500: {
      schema: InternalServerErrorSchema,
    },
  },
  list: {
    200: {
      description: 'The list of users was return successfully',
      schema: schemas.responseList,
    },
    500: {
      schema: InternalServerErrorSchema,
    },
  },
  update: {
    200: {
      description: 'The user update was occurred successfully',
      schema: schemas.response,
    },
    404: {
      description: 'The user was not found',
      schema: NotFoundErrorExceptionSchema('Update'),
    },
    503: {
      description:
        'The user update was not occurred because a service was not available',
      schema: ServiceUnavailableErrorExceptionSchema,
    },
    500: {
      schema: InternalServerErrorSchema,
    },
  },
  delete: {
    204: {
      description: 'The user was deleted successfully',
    },
    404: {
      description: 'The user was not found',
      schema: NotFoundErrorExceptionSchema('Delete'),
    },
    500: {
      schema: InternalServerErrorSchema,
    },
  },
  getByUuid: {
    200: {
      description: 'The user was return successfully',
      schema: schemas.response,
    },
    404: {
      description: 'The user was not found',
      schema: NotFoundErrorExceptionSchema('Get'),
    },
    500: {
      schema: InternalServerErrorSchema,
    },
  },
};

export { responses, schemas };
