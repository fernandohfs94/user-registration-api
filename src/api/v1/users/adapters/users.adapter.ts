import Address from '../../../../database/models/address';
import User from '../../../../database/models/user';

import { IUserResponse } from '../interfaces/users.interface';

class UsersAdapter {
  public toBusiness = (user: User, address: Address): IUserResponse => {
    return {
      id: user.id,
      uuid: user.uuid,
      login: user.login,
      fullName: user.fullName,
      document: user.document,
      active: user.active,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
      address: {
        zipCode: address.zipCode,
        street: address.street,
        number: address.number,
        district: address.district,
        city: address.city,
        state: address.state,
      },
    };
  };

  public toBusinessList = (users: User[]): Partial<IUserResponse>[] => {
    return users.map(user => ({
      id: user.id,
      uuid: user.uuid,
      login: user.login,
      fullName: user.fullName,
      document: user.document,
      active: user.active,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    }));
  };
}

export default UsersAdapter;
