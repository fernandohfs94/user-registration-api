import { Op } from 'sequelize';

import Address from '../../../database/models/address';
import User from '../../../database/models/user';
import Sequelize from '../../../database/config';
import UsersAdapter from './adapters/users.adapter';

import {
  IUserRequestDTO,
  IUserUpdateRequestDTO,
  IUserResponse,
} from './interfaces/users.interface';
import RemoveUndefined from '../../../helpers/remove-undefined';

class UsersRepository {
  public create = async ({
    login,
    password,
    fullName,
    document,
    address,
  }: IUserRequestDTO) => {
    const t = await Sequelize.getQueryInterface().sequelize.transaction();

    try {
      const createdUser = await User.create(
        {
          login,
          password,
          fullName,
          document,
        },
        {
          transaction: t,
        },
      );

      const createdAddress = await Address.create(
        {
          userId: createdUser.id,
          ...address,
        },
        {
          transaction: t,
        },
      );

      await t.commit();

      return new UsersAdapter().toBusiness(createdUser, createdAddress);
    } catch (error) {
      await t.rollback();
      throw error;
    }
  };

  public list = async (
    fullName?: string,
    login?: string,
    document?: string,
  ): Promise<Partial<IUserResponse>[]> => {
    let query = RemoveUndefined({ login, document });

    if (fullName) {
      query = { ...query, fullName: { [Op.substring]: fullName } };
    }

    const users = await User.findAll({ where: { ...query, active: true } });

    return new UsersAdapter().toBusinessList(users);
  };

  public getUserAndAddressByUuid = async (uuid: string): Promise<User> => {
    return User.findOne({
      where: { uuid, active: true },
      include: [{ model: Address }],
    });
  };

  public getById = async (id: number) => {
    return User.findOne({
      where: { id },
      include: [{ model: Address }],
    });
  };

  public update = async (
    { login, password, fullName, document, address }: IUserUpdateRequestDTO,
    userId: number,
  ): Promise<boolean> => {
    const t = await Sequelize.getQueryInterface().sequelize.transaction();

    try {
      let updated = false;

      const updateResultUser = await User.update(
        RemoveUndefined({ login, password, fullName, document }),
        {
          where: { id: userId },
          individualHooks: true,
        },
      );

      if (address) {
        const { id } = await Address.findOne({
          where: { userId },
        });

        const updatedResultAddress = await Address.update(
          RemoveUndefined(address),
          {
            where: { id },
          },
        );

        if (updateResultUser[0] > 0 && updatedResultAddress[0] > 0)
          updated = true;
      }

      if (updateResultUser[0]) updated = true;

      await t.commit();

      return updated;
    } catch (error) {
      await t.rollback();
      throw error;
    }
  };

  public delete = async (userId: number) => {
    const t = await Sequelize.getQueryInterface().sequelize.transaction();

    try {
      await User.update(
        { active: false },
        {
          where: { id: userId },
          individualHooks: true,
        },
      );

      const { id } = await Address.findOne({
        where: { userId },
      });

      await Address.update(
        { active: false },
        {
          where: { id },
        },
      );

      await t.commit();
    } catch (error) {
      await t.rollback();
      throw error;
    }
  };
}

export default UsersRepository;
