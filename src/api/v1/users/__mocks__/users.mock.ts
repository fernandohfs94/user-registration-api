const userCreatePayload = {
  login: 'ex_ample',
  password: '123456',
  fullName: 'Example of Full Name',
  document: '12345678901',
  address: {
    zipCode: '14412206',
    street: 'Rua Jovita',
    number: 462,
    district: 'Jardim das Palmeiras',
    city: 'Franca',
    state: 'SP',
  },
};

const userUpdatePayload = {
  login: 'fq_soares',
  password: '456789',
  fullName: 'Fernando Henrique Ferreira Soares',
  document: '12345678910',
  address: {
    zipCode: '14403500',
    street: 'Rua Jovita',
    number: 462,
    district: 'Jardim das Palmeiras',
    city: 'Franca',
    state: 'SP',
  },
};

const userDeletePayload = {
  login: 'ts_test',
  password: '456789',
  fullName: 'Fernando Henrique Ferreira Soares',
  document: '22558495852',
  address: {
    zipCode: '14403500',
    street: 'Rua Jovita',
    number: 462,
    district: 'Jardim das Palmeiras',
    city: 'Franca',
    state: 'SP',
  },
};

const userGetByUuidPayload = {
  login: 't_test',
  password: '456789',
  fullName: 'Teste Get By Uuid',
  document: '21558623252',
  address: {
    zipCode: '14403500',
    street: 'Rua Jovita',
    number: 462,
    district: 'Jardim das Palmeiras',
    city: 'Franca',
    state: 'SP',
  },
};

export {
  userCreatePayload,
  userUpdatePayload,
  userDeletePayload,
  userGetByUuidPayload,
};
