import { UniqueConstraintError } from 'sequelize';

import UsersRepository from './users.repository';
import UserAlreadyExistsException from './exceptions/UserAlreadyExistsException';

import {
  IUserRequestDTO,
  IUserUpdateRequestDTO,
  IUserResponse,
} from './interfaces/users.interface';
import UserNotFoundException from './exceptions/UserNotFoundException';
import UsersAdapter from './adapters/users.adapter';
import UserNotUpdatedException from './exceptions/UserNotUpdatedException';

class UsersBusiness {
  public create = async (user: IUserRequestDTO): Promise<IUserResponse> => {
    try {
      return await new UsersRepository().create(user);
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        throw new UserAlreadyExistsException(
          'You attempted to create a user with the login or document already existing and that is not possible.',
        );
      }

      throw error;
    }
  };

  public list = async (
    fullName?: string,
    login?: string,
    document?: string,
  ): Promise<Partial<IUserResponse>[]> => {
    return new UsersRepository().list(fullName, login, document);
  };

  public update = async (
    user: IUserUpdateRequestDTO,
    uuid: string,
  ): Promise<IUserResponse> => {
    const userFound = await new UsersRepository().getUserAndAddressByUuid(uuid);

    if (!userFound) {
      throw new UserNotFoundException(
        'You attempted to update a nonexistent user.',
      );
    }

    if (Object.keys(user).length > 0) {
      const userUpdated = await new UsersRepository().update(
        user,
        userFound.id,
      );

      if (userUpdated) {
        const user = await new UsersRepository().getById(userFound.id);

        return new UsersAdapter().toBusiness(user, user.address);
      }

      throw new UserNotUpdatedException('Unable to update user');
    }

    return new UsersAdapter().toBusiness(userFound, userFound.address);
  };

  public delete = async (uuid: string): Promise<void> => {
    const userFound = await new UsersRepository().getUserAndAddressByUuid(uuid);

    if (!userFound) {
      throw new UserNotFoundException(
        'You attempted to update a nonexistent user.',
      );
    }

    await new UsersRepository().delete(userFound.id);
  };

  public getByUuid = async (uuid: string): Promise<IUserResponse> => {
    const userFound = await new UsersRepository().getUserAndAddressByUuid(uuid);

    if (!userFound) {
      throw new UserNotFoundException(
        'You attempted to update a nonexistent user.',
      );
    }

    return new UsersAdapter().toBusiness(userFound, userFound.address);
  };
}

export default UsersBusiness;
