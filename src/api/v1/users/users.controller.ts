import { Request, ResponseToolkit } from '@hapi/hapi';
import HTTPStatus from 'http-status';

import UserAlreadyExistsException from './exceptions/UserAlreadyExistsException';
import UserNotFoundException from './exceptions/UserNotFoundException';
import UserNotUpdatedException from './exceptions/UserNotUpdatedException';

import UsersBusiness from './users.business';

import {
  ConflictErrorException,
  InternalServerErrorException,
  NotFoundErrorException,
  ServiceUnavailableErrorException,
} from '../../config/exception/http-exception';
import {
  IUserRequestDTO,
  IUserUpdateRequestDTO,
} from './interfaces/users.interface';

class UsersController {
  async create(request: Request, h: ResponseToolkit) {
    try {
      const user = await new UsersBusiness().create(
        request.payload as IUserRequestDTO,
      );

      return h.response(user).code(HTTPStatus.CREATED);
    } catch (error) {
      if (error instanceof UserAlreadyExistsException) {
        return h
          .response(
            new ConflictErrorException('User', error.message).getResponse(),
          )
          .code(HTTPStatus.CONFLICT);
      }

      return h
        .response(new InternalServerErrorException().getResponse())
        .code(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async list(request: Request, h: ResponseToolkit) {
    try {
      const { fullName, login, document } = request.query;

      const users = await new UsersBusiness().list(fullName, login, document);

      return h.response(users).code(HTTPStatus.OK);
    } catch (error) {
      return h
        .response(new InternalServerErrorException().getResponse())
        .code(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async update(request: Request, h: ResponseToolkit) {
    try {
      const user = await new UsersBusiness().update(
        request.payload as IUserUpdateRequestDTO,
        request.params.uuid,
      );

      return h.response(user).code(HTTPStatus.OK);
    } catch (error) {
      if (error instanceof UserNotFoundException) {
        return h
          .response(
            new NotFoundErrorException(
              request.params.uuid,
              error.message,
            ).getResponse(),
          )
          .code(HTTPStatus.NOT_FOUND);
      }

      if (error instanceof UserNotUpdatedException) {
        return h
          .response(
            new ServiceUnavailableErrorException(error.message).getResponse(),
          )
          .code(HTTPStatus.SERVICE_UNAVAILABLE);
      }

      return h
        .response(new InternalServerErrorException().getResponse())
        .code(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async delete(request: Request, h: ResponseToolkit) {
    try {
      await new UsersBusiness().delete(request.params.uuid);

      return h.response().code(HTTPStatus.NO_CONTENT);
    } catch (error) {
      if (error instanceof UserNotFoundException) {
        return h
          .response(
            new NotFoundErrorException(
              request.params.uuid,
              error.message,
            ).getResponse(),
          )
          .code(HTTPStatus.NOT_FOUND);
      }

      return h
        .response(new InternalServerErrorException().getResponse())
        .code(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getByUuid(request: Request, h: ResponseToolkit) {
    try {
      const user = await new UsersBusiness().getByUuid(request.params.uuid);

      return h.response(user).code(HTTPStatus.OK);
    } catch (error) {
      if (error instanceof UserNotFoundException) {
        return h
          .response(
            new NotFoundErrorException(
              request.params.uuid,
              error.message,
            ).getResponse(),
          )
          .code(HTTPStatus.NOT_FOUND);
      }

      return h
        .response(new InternalServerErrorException().getResponse())
        .code(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

export default new UsersController();
