import HTTPStatus from 'http-status';

import Server from '../../../../server';
import User from '../../../../database/models/user';

import { IUserResponse } from '../interfaces/users.interface';
import {
  userCreatePayload,
  userDeletePayload,
  userUpdatePayload,
  userGetByUuidPayload,
} from '../__mocks__/users.mock';

describe('Route /users', () => {
  const server = Server.getInstance().getServer();

  beforeAll(done => {
    server.events.on('start', () => {
      done();
    });
  });

  afterAll(done => {
    server.events.on('stop', () => {
      done();
    });

    server.stop();
  });

  test('POST / - create a new user successfully', async () => {
    const options = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userCreatePayload),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.CREATED);
  });

  test('POST / - handle conflict error exception', async () => {
    const options = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userCreatePayload),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.CONFLICT);
  });

  test('POST / - handle internal server error exception', async () => {
    const spyUserCreate = jest.spyOn(User, 'create');
    spyUserCreate.mockImplementation(() => {
      throw new Error();
    });

    const options = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userCreatePayload),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.INTERNAL_SERVER_ERROR);

    spyUserCreate.mockRestore();
  });

  test('GET / - list users successfully', async () => {
    const options = {
      method: 'GET',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userCreatePayload),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.OK);
    expect(response.payload.length).toBeGreaterThan(0);
  });

  test('GET / - list users querying by fullName successfully', async () => {
    const options = {
      method: 'GET',
      url: '/v1/users?fullName=of',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userCreatePayload),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.OK);
    expect(response.payload.length).toBeGreaterThan(0);
  });

  test('GET / - list users querying by login successfully', async () => {
    const options = {
      method: 'GET',
      url: '/v1/users?login=ex_ample',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userCreatePayload),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.OK);
    expect(response.payload.length).toBeGreaterThan(0);
  });

  test('GET / - list users querying by document successfully', async () => {
    const options = {
      method: 'GET',
      url: '/v1/users?document=12345678901',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userCreatePayload),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.OK);
    expect(response.payload.length).toBeGreaterThan(0);
  });

  test('GET / - handle internal server error exception', async () => {
    const spyUserFindAll = jest.spyOn(User, 'findAll');
    spyUserFindAll.mockImplementation(() => {
      throw new Error();
    });

    const options = {
      method: 'GET',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userCreatePayload),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.INTERNAL_SERVER_ERROR);

    spyUserFindAll.mockRestore();
  });

  test('PATCH / - update an user successfully', async () => {
    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userUpdatePayload),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsPatch = {
      method: 'PATCH',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({ ...userUpdatePayload, fullName: 'Testing' }),
    };

    const responsePatch = await server.inject(optionsPatch);
    const updatedUser = JSON.parse(responsePatch.payload) as IUserResponse;

    expect(responsePatch.statusCode).toBe(HTTPStatus.OK);
    expect(updatedUser.fullName).toBe('Testing');
  });

  test('PATCH / - not update an user and return his data successfully', async () => {
    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({
        ...userUpdatePayload,
        login: 'test_1',
        document: '35265498258',
      }),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsPatch = {
      method: 'PATCH',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({}),
    };

    const responsePatch = await server.inject(optionsPatch);
    const updatedUser = JSON.parse(responsePatch.payload) as IUserResponse;

    expect(responsePatch.statusCode).toBe(HTTPStatus.OK);
    expect(updatedUser.login).toBe('test_1');
    expect(updatedUser.fullName).toBe(userUpdatePayload.fullName);
    expect(updatedUser.document).toBe('35265498258');
  });

  test('PATCH / - update an user and his address successfully', async () => {
    const userMock = {
      ...userUpdatePayload,
      login: 'test_2',
      document: '35265647258',
    };

    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userMock),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsPatch = {
      method: 'PATCH',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({
        ...userMock,
        fullName: 'Testing',
        address: {
          ...userMock.address,
          street: 'Street of test',
          number: 123,
        },
      }),
    };

    const responsePatch = await server.inject(optionsPatch);
    const updatedUser = JSON.parse(responsePatch.payload) as IUserResponse;

    expect(responsePatch.statusCode).toBe(HTTPStatus.OK);
    expect(updatedUser.fullName).toBe('Testing');
    expect(updatedUser.document).toBe('35265647258');
    expect(updatedUser.address.street).toBe('Street of test');
    expect(updatedUser.address.number).toBe(123);
  });

  test('PATCH / - handle not found error exception', async () => {
    const options = {
      method: 'PATCH',
      url: `/v1/users/ea70346c-2a16-4d67-b08d-d873d5ec6b2d`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({ ...userUpdatePayload, fullName: 'Testing' }),
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.NOT_FOUND);
  });

  test('PATCH / - handle service unavailable error exception', async () => {
    const spyUserUpdate = jest.spyOn(User, 'update');
    spyUserUpdate.mockResolvedValue([0, []]);

    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({
        ...userUpdatePayload,
        login: 'test_3',
        document: '32165498722',
      }),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsPatch = {
      method: 'PATCH',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({ ...userUpdatePayload, fullName: 'Testing' }),
    };

    const responsePatch = await server.inject(optionsPatch);

    expect(responsePatch.statusCode).toBe(HTTPStatus.SERVICE_UNAVAILABLE);

    spyUserUpdate.mockRestore();
  });

  test('PATCH / - handle internal server error exception', async () => {
    const spyUserUpdate = jest.spyOn(User, 'update');
    spyUserUpdate.mockImplementation(() => {
      throw new Error();
    });

    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({
        ...userUpdatePayload,
        login: 'test_4',
        document: '33365498755',
      }),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsPatch = {
      method: 'PATCH',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({ ...userUpdatePayload, fullName: 'Testing' }),
    };

    const responsePatch = await server.inject(optionsPatch);

    expect(responsePatch.statusCode).toBe(HTTPStatus.INTERNAL_SERVER_ERROR);

    spyUserUpdate.mockRestore();
  });

  test('DELETE / - delete an user successfully', async () => {
    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userDeletePayload),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsDelete = {
      method: 'DELETE',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
    };

    const responseDelete = await server.inject(optionsDelete);

    expect(responseDelete.statusCode).toBe(HTTPStatus.NO_CONTENT);
  });

  test('DELETE / - handle not found error exception', async () => {
    const options = {
      method: 'DELETE',
      url: '/v1/users/ea70346c-2a16-4d67-b08d-d873d5ec6b2d',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.NOT_FOUND);
  });

  test('DELETE / - handle internal server error exception', async () => {
    const spyUserUpdate = jest.spyOn(User, 'update');
    spyUserUpdate.mockImplementation(() => {
      throw new Error();
    });

    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({
        ...userDeletePayload,
        login: 'test_5',
        document: '31968258722',
      }),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsDelete = {
      method: 'DELETE',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
    };

    const responseDelete = await server.inject(optionsDelete);

    expect(responseDelete.statusCode).toBe(HTTPStatus.INTERNAL_SERVER_ERROR);

    spyUserUpdate.mockRestore();
  });

  test('GET by UUID / - get an user by uuid successfully', async () => {
    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify(userGetByUuidPayload),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsGetByUuid = {
      method: 'GET',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
    };

    const responseGetByUuid = await server.inject(optionsGetByUuid);
    const user = JSON.parse(responseGetByUuid.payload) as IUserResponse;

    expect(responseGetByUuid.statusCode).toBe(HTTPStatus.OK);
    expect(user.login).toBe(userGetByUuidPayload.login);
    expect(user.fullName).toBe(userGetByUuidPayload.fullName);
    expect(user.document).toBe(userGetByUuidPayload.document);
    expect(user.address.city).toBe(userGetByUuidPayload.address.city);
  });

  test('GET by UUID / - handle not found error exception', async () => {
    const options = {
      method: 'GET',
      url: '/v1/users/ea70346c-2a16-4d67-b08d-d873d5ec6b2d',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
    };

    const response = await server.inject(options);

    expect(response.statusCode).toBe(HTTPStatus.NOT_FOUND);
  });

  test('GET by UUID / - handle internal server error exception', async () => {
    const spyUserFindOne = jest.spyOn(User, 'findOne');
    spyUserFindOne.mockImplementation(() => {
      throw new Error();
    });

    const optionsPost = {
      method: 'POST',
      url: '/v1/users',
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
      payload: JSON.stringify({
        ...userDeletePayload,
        login: 'test_6',
        document: '87969525714',
      }),
    };

    const responsePost = await server.inject(optionsPost);
    const createdUser = JSON.parse(responsePost.payload) as IUserResponse;

    const optionsGetByUuid = {
      method: 'GET',
      url: `/v1/users/${createdUser.uuid}`,
      headers: {
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Imp3dERldmVsb3BtZW50IiwiaWF0IjoxNjI0MzExMTQ0fQ.3GmRDeGgJp2UkvPso_a38KwimlUsoKYsPihB3JdBicQ',
      },
    };

    const responseGetByUuid = await server.inject(optionsGetByUuid);

    expect(responseGetByUuid.statusCode).toBe(HTTPStatus.INTERNAL_SERVER_ERROR);

    spyUserFindOne.mockRestore();
  });
});
