import { ServerRoute } from '@hapi/hapi';
import Joi from 'joi';

import UsersController from './users.controller';
import { responses, schemas } from './users.schema';

const usersRoutes: ServerRoute[] = [
  {
    method: 'POST',
    path: '/v1/users',
    options: {
      auth: 'jwt',
      handler: UsersController.create,
      description: 'Create a new user',
      tags: ['api', 'users'],
      validate: {
        payload: schemas.request,
      },
      plugins: {
        'hapi-swagger': {
          responses: responses.create,
        },
      },
    },
  },
  {
    method: 'GET',
    path: '/v1/users',
    options: {
      auth: 'jwt',
      handler: UsersController.list,
      description: 'List all users',
      tags: ['api', 'users'],
      validate: {
        query: schemas.query,
      },
      plugins: {
        'hapi-swagger': {
          responses: responses.list,
        },
      },
    },
  },
  {
    method: 'PATCH',
    path: '/v1/users/{uuid}',
    options: {
      auth: 'jwt',
      handler: UsersController.update,
      description: 'Update an user',
      tags: ['api', 'users'],
      validate: {
        payload: schemas.requestUpdate,
        params: Joi.object({
          uuid: Joi.string().uuid(),
        }),
      },
      plugins: {
        'hapi-swagger': {
          responses: responses.update,
        },
      },
    },
  },
  {
    method: 'DELETE',
    path: '/v1/users/{uuid}',
    options: {
      auth: 'jwt',
      handler: UsersController.delete,
      description: 'Delete an user',
      tags: ['api', 'users'],
      validate: {
        params: Joi.object({
          uuid: Joi.string().uuid(),
        }),
      },
      plugins: {
        'hapi-swagger': {
          responses: responses.delete,
        },
      },
    },
  },
  {
    method: 'GET',
    path: '/v1/users/{uuid}',
    options: {
      auth: 'jwt',
      handler: UsersController.getByUuid,
      description: 'Get an user by uuid',
      tags: ['api', 'users'],
      validate: {
        params: Joi.object({
          uuid: Joi.string().uuid(),
        }),
      },
      plugins: {
        'hapi-swagger': {
          responses: responses.getByUuid,
        },
      },
    },
  },
];

export default usersRoutes;
