import { ServerRegisterPluginObject } from '@hapi/hapi';
import HapiSwagger, { RegisterOptions } from 'hapi-swagger';
import Inert from '@hapi/inert';
import Vision from '@hapi/vision';

const options: RegisterOptions = {
  basePath: '/v1/',
  info: {
    title: 'user-registration-api',
    version: 'v1.0.0',
  },
  grouping: 'tags',
  tags: [
    {
      name: 'users',
      description: 'Everything about the users',
    },
  ],
  securityDefinitions: {
    jwt: {
      type: 'apiKey',
      name: 'Authorization',
      in: 'header',
    },
  },
  security: [{ jwt: [] }],
};

export const swaggerPlugins: Array<ServerRegisterPluginObject<any>> = [
  {
    plugin: Inert,
  },
  {
    plugin: Vision,
  },
  {
    plugin: HapiSwagger,
    options,
  },
];
