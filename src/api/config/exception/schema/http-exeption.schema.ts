import Joi from 'joi';
import HTTPStatus from 'http-status';

const InternalServerErrorSchema = Joi.object({
  developerMessage: Joi.string().default('Internal Server Error'),
  errorCode: Joi.number().default(1000),
  statusCode: Joi.number().default(HTTPStatus.INTERNAL_SERVER_ERROR),
  userMessage: Joi.string().default(
    'Was encountered an error when processing your request. We apologize for the inconvenience.',
  ),
}).label('Internal Server Error');

const ConflictErrorExceptionSchema = Joi.object({
  developerMessage: Joi.string().default('User already exists'),
  userMessage: Joi.string().default(
    'You attempted to create a user with the login or document already existing and that is not possible.',
  ),
  errorCode: Joi.number().default(20033),
  statusCode: Joi.number().default(HTTPStatus.CONFLICT),
}).label('Conflict Error');

const NotFoundErrorExceptionSchema = (action: 'Delete' | 'Get' | 'Update') => {
  return Joi.object({
    developerMessage: Joi.string().default(
      'aaf3b99d-d786-4fe1-b055-6985a151f8d1 not found',
    ),
    userMessage: Joi.string().default(
      `You attempted to ${action} a nonexistent user.`,
    ),
    errorCode: Joi.number().default(20023),
    statusCode: Joi.number().default(HTTPStatus.NOT_FOUND),
  }).label(`Not Found Error on ${action}`);
};

const ServiceUnavailableErrorExceptionSchema = Joi.object({
  developerMessage: Joi.string().default('Service Unavailable'),
  userMessage: Joi.string().default(
    'Unable to update user because a service is unavailable',
  ),
  errorCode: Joi.number().default(20043),
  statusCode: Joi.number().default(HTTPStatus.SERVICE_UNAVAILABLE),
}).label('Service Unavailable Error');

export {
  InternalServerErrorSchema,
  ConflictErrorExceptionSchema,
  NotFoundErrorExceptionSchema,
  ServiceUnavailableErrorExceptionSchema,
};
