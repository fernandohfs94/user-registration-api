interface Payload {
  developerMessage: string;
  errorCode: number;
  statusCode: number;
  userMessage: string;
}

abstract class BaseException extends Error {
  payload: Payload;

  constructor(payload: Payload) {
    super(JSON.stringify({ ...payload }));
    this.payload = payload;
  }

  getResponse() {
    return {
      ...this.payload,
      moreInfo: 'http://www.developer.apiluiza.com.br/errors',
      errorName: this.constructor.name,
    };
  }
}

export default BaseException;
