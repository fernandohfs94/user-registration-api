import HTTPStatus from 'http-status';

import BaseException from './base-exception';

class InternalServerErrorException extends BaseException {
  constructor() {
    super({
      developerMessage: 'Internal server error',
      errorCode: 10000,
      statusCode: HTTPStatus.INTERNAL_SERVER_ERROR,
      userMessage:
        'Was encountered an error when processing your request. We apologize for the inconvenience.',
    });
  }
}

class ConflictErrorException extends BaseException {
  constructor(origin: string, message: string) {
    super({
      developerMessage: `${origin} already exists`,
      userMessage: message,
      errorCode: 20033,
      statusCode: HTTPStatus.CONFLICT,
    });
  }
}

class NotFoundErrorException extends BaseException {
  constructor(origin: string, message: string) {
    super({
      developerMessage: `${origin} not found`,
      userMessage: message,
      errorCode: 20023,
      statusCode: HTTPStatus.NOT_FOUND,
    });
  }
}

class ServiceUnavailableErrorException extends BaseException {
  constructor(message: string) {
    super({
      developerMessage: `Service Unavailable`,
      userMessage: `${message}, because a service is unavailable`,
      errorCode: 20023,
      statusCode: HTTPStatus.SERVICE_UNAVAILABLE,
    });
  }
}

export {
  InternalServerErrorException,
  ConflictErrorException,
  NotFoundErrorException,
  ServiceUnavailableErrorException,
};
