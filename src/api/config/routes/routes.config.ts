import healthRoutes from '../../v1/health/health.routes';
import usersRoutes from '../../v1/users/users.routes';

export const routes = [].concat(healthRoutes, usersRoutes);
