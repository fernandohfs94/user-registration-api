import DataTypes, { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface, Sequelize: typeof DataTypes) =>
    queryInterface.createTable('user', {
      id: {
        field: 'id',
        type: Sequelize.INTEGER(),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      uuid: {
        field: 'uuid',
        type: Sequelize.STRING(36),
      },
      login: {
        field: 'login',
        type: Sequelize.STRING(20),
      },
      password: {
        field: 'password',
        type: Sequelize.STRING(100),
      },
      fullName: {
        field: 'full_name',
        type: Sequelize.STRING(100),
      },
      document: {
        field: 'document',
        type: Sequelize.STRING(11),
      },
      active: {
        field: 'active',
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
        allowNull: true,
      },
    }),

  down: (queryInterface: QueryInterface) => queryInterface.dropTable('user'),
};
