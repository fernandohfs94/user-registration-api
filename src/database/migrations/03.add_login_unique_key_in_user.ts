import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.addConstraint('user', {
      fields: ['login'],
      type: 'unique',
      name: 'uk_login',
    });
  },

  down: (queryInterface: QueryInterface) =>
    queryInterface.removeConstraint('user', 'uk_login'),
};
