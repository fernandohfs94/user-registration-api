import DataTypes, { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface, Sequelize: typeof DataTypes) =>
    queryInterface.createTable('address', {
      id: {
        field: 'id',
        type: Sequelize.INTEGER(),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      uuid: {
        field: 'uuid',
        type: Sequelize.STRING(36),
      },
      userId: {
        field: 'user_id',
        type: Sequelize.INTEGER(),
        allowNull: false,
        references: {
          model: 'user',
          key: 'id',
        },
      },
      zipCode: {
        field: 'zip_code',
        type: Sequelize.STRING(8),
      },
      street: {
        field: 'street',
        type: Sequelize.STRING(200),
      },
      number: {
        field: 'number',
        type: Sequelize.INTEGER,
      },
      district: {
        field: 'district',
        type: Sequelize.STRING(200),
      },
      city: {
        field: 'city',
        type: Sequelize.STRING(100),
      },
      state: {
        field: 'state',
        type: Sequelize.STRING(2),
      },
      active: {
        field: 'active',
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
        allowNull: true,
      },
    }),

  down: (queryInterface: QueryInterface) => queryInterface.dropTable('address'),
};
