import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.addConstraint('user', {
      fields: ['document'],
      type: 'unique',
      name: 'uk_document',
    });
  },

  down: (queryInterface: QueryInterface) =>
    queryInterface.removeConstraint('user', 'uk_document'),
};
