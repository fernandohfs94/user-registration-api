import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.bulkInsert('user', [
      {
        id: 1,
        uuid: '677e8fdd-6ce0-4149-abe8-c9cc0f57daf3',
        login: 'test',
        password: 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',
        full_name: 'Testing',
        document: '11111111111',
        active: true,
        created_at: new Date('1986-03-22'),
        updated_at: new Date('1986-03-22'),
      },
    ]);
  },

  down: (queryInterface: QueryInterface) => {
    return queryInterface.bulkDelete('user', {}, {});
  },
};
