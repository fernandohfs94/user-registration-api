import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.bulkInsert('address', [
      {
        id: 1,
        uuid: '31887d1f-add0-4ffd-801f-44d56efe31c9',
        user_id: 1,
        zip_code: '11111111',
        street: 'Rua do Teste',
        number: 123,
        district: 'Jardim do Teste',
        city: 'Teste',
        state: 'TT',
        active: true,
        created_at: new Date('1986-03-22'),
        updated_at: new Date('1986-03-22'),
      },
    ]);
  },

  down: (queryInterface: QueryInterface) => {
    return queryInterface.bulkDelete('address', {}, {});
  },
};
