import {
  Column,
  Model,
  PrimaryKey,
  Table,
  CreatedAt,
  UpdatedAt,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
  Default,
} from 'sequelize-typescript';
import { v4 as uuidv4 } from 'uuid';

import User from './user';

@Table({ tableName: 'address' })
class Address extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id: number;

  @Default(uuidv4)
  @Column
  public uuid: string;

  @ForeignKey(() => User)
  @Column
  public userId: number;

  @Column
  public zipCode: string;

  @Column
  public street: string;

  @Column
  public number: number;

  @Column
  public district: string;

  @Column
  public city: string;

  @Column
  public state: string;

  @Default(true)
  @Column
  public active: boolean;

  @CreatedAt
  @Column
  createdAt: Date;

  @UpdatedAt
  @Column
  updatedAt: Date;

  @BelongsTo(() => User)
  user: User;
}

export default Address;
