import bcrypt from 'bcryptjs';
import {
  Table,
  Column,
  Model,
  PrimaryKey,
  CreatedAt,
  UpdatedAt,
  AutoIncrement,
  Default,
  BeforeUpdate,
  BeforeCreate,
  HasOne,
} from 'sequelize-typescript';
import { v4 as uuidv4 } from 'uuid';
import Address from './address';

@Table({ tableName: 'user' })
class User extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id: number;

  @Default(uuidv4)
  @Column
  public uuid: string;

  @Column
  public login: string;

  @Column
  public password: string;

  @Column
  public fullName: string;

  @Column
  public document: string;

  @Default(true)
  @Column
  public active: boolean;

  @CreatedAt
  @Column
  public createdAt: Date;

  @UpdatedAt
  @Column
  public updatedAt: Date;

  @HasOne(() => Address)
  public address: Address;

  @BeforeCreate
  static hashPasswordBeforeCreate(instance: User): void {
    // eslint-disable-next-line no-param-reassign
    instance.password = bcrypt.hashSync(instance.password, 10);
  }

  @BeforeUpdate
  static hashPasswordBeforeUpdate(instance: User): void {
    if (instance.password) {
      // eslint-disable-next-line no-param-reassign
      instance.password = bcrypt.hashSync(instance.password, 10);
    }
  }
}

export default User;
