import { Sequelize as SequelizeTS } from 'sequelize-typescript';

import Env from '../../config/env.config';

const Sequelize = new SequelizeTS({
  database: Env.DB_NAME,
  host: Env.DB_HOST,
  username: Env.DB_USERNAME,
  password: Env.DB_PASSWORD,
  dialect: 'mysql',
  models: [`${__dirname}/../models`],
  define: {
    timestamps: false,
    underscored: true,
  },
  logging: Env.NODE_ENV === 'development',
});

export default Sequelize;
