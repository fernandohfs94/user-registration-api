import Env from '../../config/env.config';

const Database = {
  username: Env.DB_USERNAME,
  password: Env.DB_PASSWORD,
  database: Env.DB_NAME,
  host: Env.DB_HOST,
  port: Env.DB_PORT,
  dialect: 'mysql',
};

module.exports = {
  [Env.NODE_ENV]: Database,
};
