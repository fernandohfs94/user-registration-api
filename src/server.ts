import Hapi, { ResponseToolkit } from '@hapi/hapi';
import HapiAuthJwt from 'hapi-auth-jwt2';

import Env from './config/env.config';
import generateToken from './helpers/generate-token.helper';

import { routes } from './api/config/routes/routes.config';
import { swaggerPlugins } from './api/config/swagger/swagger.config';

import './database/config';

class Server {
  private server: Hapi.Server;
  private static instance: Server;

  private constructor() {
    this.server = new Hapi.Server({
      host: Env.HOST,
      port: Env.PORT,
      routes: {
        cors: {
          origin: ['*'],
        },
      },
    });
  }

  public static getInstance(): Server {
    if (!Server.instance) {
      Server.instance = new Server();
    }

    return Server.instance;
  }

  public async start(): Promise<void> {
    await this.registerPlugins();
    await this.loadAuthentication();
    await this.loadRoutes();

    await this.server.start();

    console.log(`Server running at ${this.server.info.uri}`);
  }

  private async registerPlugins(): Promise<void> {
    await this.server.register([...swaggerPlugins, { plugin: HapiAuthJwt }]);
  }

  private async loadAuthentication(): Promise<void> {
    generateToken();

    this.server.auth.strategy('jwt', 'jwt', {
      key: Env.JWT_SECRET_KEY,
      validate: async (
        decoded: { token: string },
        request: Request,
        h: ResponseToolkit,
      ) => ({
        isValid: decoded.token === Env.JWT_TOKEN,
      }),
    });

    this.server.auth.default('jwt');
  }

  private async loadRoutes(): Promise<void> {
    this.server.route(routes);
  }

  public getServer(): Hapi.Server {
    return this.server;
  }
}

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

Server.getInstance().start();

export default Server;
