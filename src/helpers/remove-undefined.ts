// eslint-disable-next-line @typescript-eslint/ban-types
const RemoveUndefined = (obj: object) => {
  return JSON.parse(JSON.stringify(obj));
};

export default RemoveUndefined;
