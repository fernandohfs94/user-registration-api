import jwt from 'jsonwebtoken';

import Env from '../config/env.config';

const generateToken = () => {
  return jwt.sign({ token: Env.JWT_TOKEN }, Env.JWT_SECRET_KEY);
};

export default generateToken;
