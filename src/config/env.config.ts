import dotEnv from 'dotenv';

dotEnv.config();

const {
  NODE_ENV,
  HOST,
  PORT,
  JWT_TOKEN,
  JWT_SECRET_KEY,
  DB_NAME,
  DB_HOST,
  DB_PORT,
  DB_USERNAME,
  DB_PASSWORD,
} = process.env;

export default {
  NODE_ENV,
  HOST,
  PORT,
  JWT_TOKEN,
  JWT_SECRET_KEY,
  DB_NAME,
  DB_HOST,
  DB_PORT,
  DB_USERNAME,
  DB_PASSWORD,
};
